// env vars

const WEBAPP_PORT = process.env.WEBAPP_PORT || 5000;

// deps

const http = require('http');
const {Client, Pool} = require('pg');

const connect = require('connect');
const bodyParser = require('body-parser');
const compression = require('compression');

// postgres

let pool;
try {
    pool = new Pool({connectionString: process.env.DATABASE_URL});
    pool.query('create table if not exists visitors (entry varchar(255))');
}
catch (error) {
    console.log(`🛑 Postgres: Failed to connect or migrate`);
    console.error(error);
}

// main

const server_app = connect();

server_app.use(compression());
server_app.use(bodyParser.urlencoded({extended: false}));

server_app.use((req, res) => {
    (async () => {
        try {
            const entry = (new Date()).toISOString();
            await pool.query(`insert into visitors (entry) values ('${entry}')`);
            const list_stuff = await pool.query('select entry from visitors');
            console.log(list_stuff.rows);
            let response = list_stuff.rows.map(row => '-' + JSON.stringify(row)).join('\n');
            response += '\n'
            response += JSON.stringify(process.env);
            res.end(response);
        }
        catch (error) {
            console.log(`🛑 Failed to insert or select`);
            console.error(error);
            res.end('Something gone bad.');
        }
    })();
});

http.createServer(server_app).listen(WEBAPP_PORT);
