FROM node:14

COPY webapp .

RUN npm install

EXPOSE 5000

CMD ["npm", "run", "start", "--host", "0.0.0.0"]
